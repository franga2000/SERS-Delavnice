<?php
require_once("include/init.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		
		<title><?php echo $config["šola"] ?></title>
		
		<link rel="icon" href="edit/favicon.ico"/>
		<link rel="stylesheet" href="edit/themes/default/reset.css">
		<link rel="stylesheet" href="edit/themes/default/screen.css">
		<link rel="stylesheet" href="edit/themes/default/fonts.css">
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<div id="installer">
			<h1>SERŠ delavnice: <?php echo $config["šola"] ?></h1>
			<?php if ($config["odprto"] && !empty($config["šola"])):
				/*
					Delavnice odprte
				*/ ?>
			<?php if (!isset($_COOKIE["register"])) { 
					/* 
						Uporabnik ni registriran
					*/ ?>
				<form id="login" method="post" action="include/registracija.php">
					<label>Registracija:</label>
					<p style="color:#AAA;">Dovoljene so samo črke in presledki</p><br>
					
					<label><span class="icon-user login-icon"></span> Ime in priimek</label>
					<input name="ime" type="text" autocomplete="off" autofocus required>
					
					<button type="submit">Registracija</button>
				</form>
			</form>
			<br><br>
			<?php } 
				/* Seznam strani */ ?>
			<a href="edit/" style="float:right;">Urejevalnik</a>
			<label>Seznam strani:</label>
			<ul> <?php
				foreach (getDirs("./", $config["ignore"]) as $dir) { ?>
					<li><a href="<?php echo $dir ?>"><?php echo $dir ?></a></li><?php 
				} ?>
			</ul>
			<style>
				li {
					margin-top: 5px;
					margin-left: 25px;
				}
				a {
					font-size: 14pt;
					color: #DDD;
					text-decoration: none;
				}
				a:hover, a:active {
					color: #FFF;
					text-decoration: underline;
				}
			</style>
			
			<?php else: 
				/*
					Delavnice zaprte
				*/ ?>
				<?php if (!empty($config["šola"])) { 
					/* 
						Delavnice so nameščene
					*/ ?>
				<h1>Delavnice so zaprte</h1>
				<label>Več info na <a href="https://pridina.sers.si">https://pridina.sers.si</a></label><br>
				<?php } else { 
					/* 
						Delavnice niso namešče
					*/ 
				?>
				<style>
				code {
					background: rgba(255,255,255,.1);
					padding: 1px 3px;
					font-family: monospace;
				}
				</style>
				<?php
				if (!is_writeable(".")) die("Nimam dovoljenja za pisanje v mapo!<br><code>chmod g+w .</code> or yell at Frangež");
				?>
				<form method="POST" action="include/install.php">	
					<h1>Namestitev</h1>
					
					<label>Ime šole</label>
					<input name="šola" type="text" autofocus required>
					
					<label>Predloga</label>
					<select name="predloga" required><?php
						foreach (getDirs("include/", $config["ignore"]) as $dir) { ?>
							<option value="<?php echo $dir ?>"><?php echo $dir ?></option><?php 
						} ?>
					</select><br>
					
					<label>Geslo šole</label>
					<input name="geslo" type="text" required>
					
					<label>Administratorsko geslo</label>
					<input name="admin_geslo" type="password" required>
					
					<button type="submit">Odpri!</button>
				</form>
				<?php } ?>
			<?php endif ?>
		</div>
	</body>
</html>
