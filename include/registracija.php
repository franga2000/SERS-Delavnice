<?php
require_once("init.php");

if ($_SERVER["REQUEST_METHOD"] === "POST") {
	
	// Preflight:
	if (!$config["odprto"]) die("Registracija zaprta!");
	if (isset($_COOKIE["register"])) die("Ste že registrirani");
	
	// Preveri vnešeno ime
	$user = trim($_POST["ime"]);
	if (!validate($user)) die("Nedovoljeni znaki v '" . $user . "'!");
	if (file_exists("../" . $user)) die("Stran že obstaja!");
	
	// Preveri geslo
	$geslo = $config["geslo"];
	$admin = false;
	if (isset($_POST["geslo"]) && $_POST["geslo"] == $config["admin_geslo"]) $admin = true;
	
	/*if ($_POST["geslo"] != $config["geslo"] && !$admin) die("Napačno geslo!");
	$geslo = $_POST["geslo"];*/
	
	// Kopiraj predlogo
	$success = mkdir("../" . $user);
	if ($success) $succes = recurse_copy($config["predloga"], "../" . $user);
	else die("Napaka pri kopiranju predloge");

	// Omeji uporabnika na njihov projekt
	if (!$admin) {
		$success = file_put_contents("../edit/data/" . $user . "_acl.php", codiad_encode(array($user)));
		if (!$success) die("Napaka pri ustvarjanju uporabnika");
	}
	
	// Ustvari uporabnika
	$users = codiad_decode(file_get_contents("../edit/data/users.php"));
	array_push($users, array(
		"username" => $user,
		"password" => sha1(md5($geslo)),
		"project" => $user
	));
	file_put_contents("../edit/data/users.php", codiad_encode($users));
	
	// Ustvari projekt
	$projects = codiad_decode(file_get_contents("../edit/data/projects.php"));
	array_push($projects, array(
		"name" => $user,
		"path" => $user
	));
	file_put_contents("../edit/data/projects.php", codiad_encode($projects));
	
	// Prepreči ponovno registracijo
	setcookie("register", $user, 0, "/");
	
	// Prijavi uporabnika v Codiad
	session_name(md5(realpath("../edit/")));
	session_start();
	$_SESSION['user'] = $user;
	$_SESSION['lang'] = "en";
	$_SESSION['theme'] = "default";
	$_SESSION['project'] = $user;
	
	// Preusmeri uporabnika na njihovo stran
	header("Location: ../");
	
} ?>
