<?php
$config = codiad_decode(file_get_contents(__DIR__ . "/../config.php"));
if (empty($config)) die("Config error");

function getDirs($dir, $ignore = array()) {
	$dirs = array();
	
	$things = scandir($dir);
	foreach ($things as $thing) {
		if (!in_array($thing, $ignore) && is_dir($dir . $thing)) {
			array_push($dirs, $thing);
		}
	}
	return $dirs;
}

function codiad_encode($data) {
	return "<?php/*|" . json_encode($data,  JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "|*/?>";
}

function codiad_decode($string) {
	$string = str_replace("<?php/*|", "", $string);
	$string = str_replace("|*/?>", "", $string);
	if (empty($string)) return array();
	else return json_decode($string, true);
}

function str_contains($string, $array) {
	foreach ($array as $str) {
		if (strpos($string, $str) !== false) return true;
	}
	return false;
}

function validate($string) {
	$string = str_replace(" ", "", $string);
	if (empty($string)) return false;
	if (str_contains($string, array("/", ":", "'", '"', "#"))) return false;
	//if (!preg_match("/^([\p{L} ]+)$/", $string)) return false;
	return true;
}

function recurse_copy($source, $dest){
    if(is_dir($source)) {
        $dir_handle=opendir($source);
        while($file=readdir($dir_handle)){
            if($file!="." && $file!=".."){
                if(is_dir($source."/".$file)){
                    if(!is_dir($dest."/".$file)){
                        mkdir($dest."/".$file);
                    }
                    recurse_copy($source."/".$file, $dest."/".$file);
                } else {
                    copy($source."/".$file, $dest."/".$file);
                }
            }
        }
        closedir($dir_handle);
    } else {
        copy($source, $dest);
    }
}
?>
