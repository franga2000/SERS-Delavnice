function getPage() {
	var hash = window.location.hash.replace("#", "");
	var page = hash.length > 1 ? hash : "o-meni";
	return page;
}

window.onload = function() {
	loadPage(getPage());
	
	if (window.onhashchange === undefined) {
		onclick = function(e) {
			loadPage(e.href);
		}
	} else {
		window.onhashchange = function() {
			loadPage(getPage());
		}
	}
}

function loadPage(page) {
	console.info("Loading " + page);
	var html = document.getElementById("content-" + page).innerHTML;
	document.getElementsByTagName("main")[0].innerHTML = html;
	document.getElementById("main-nav").getElementsByClassName("active")[0].classList = "";
	document.getElementById("nav-" + page).classList = "active";
	document.getElementsByTagName("main")[0].id = page;
}