<?php
require_once("init.php");

if ($_SERVER["REQUEST_METHOD"] === "POST") {
	
	// Preflight:
	if (!empty($config["šola"])) die("Stran je že nameščena!");
	
	// Preveri admin geslo
	if ($_POST["admin_geslo"] != $config["admin_geslo"]) die("You tried :D");
	
	// Preveri vnešeno ime
	$šola = trim($_POST["šola"]);
	if (!validate($šola)) die("Nedovoljeni znaki!");
	
	// Preveri vnešeno predlogo
	$predloga = trim($_POST["predloga"]);
	if (!validate($predloga)) die("Nedovoljeni znaki!");
	if (!file_exists($predloga)) die("Predloga '" . $predloga . "' ne obstaja!");
	
	// Preveri vnešeno ime
	$geslo = trim($_POST["geslo"]);
	if (!validate($geslo)) die("Nedovoljeni znaki!");
	
	// Vpiši podatke v config
	$config["šola"] = $šola;
	$config["predloga"] = $predloga;
	$config["geslo"] = $geslo;
	file_put_contents("../config.php", codiad_encode($config));
	
	// Preusmeri na glavno stran
	header("Location: ../");
	
} ?>
